package com.example.SpringBootDeviceDB.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "devices")
public class Device {
    @Id
    @GeneratedValue(generator = "device_generator")
    @SequenceGenerator(
            name = "device_generator",
            sequenceName = "device_sequence",
            initialValue = 1000
    )
    private Long deviceId;

    @NotBlank
    @Size(min = 3, max = 100)
    private String name;

    private String scentType;

    @Column(columnDefinition = "text")
    private String scentVolume;

    // Getters and Setters (Omitted for brevity)
    public long getdeviceId() {
        return deviceId;
    }
    public void setdeviceId(long deviceId) {
        this.deviceId = deviceId;
    }
    public String getName() {
        return name;
    }
    public void setName(String Name) {
        this.name = name;
    }

    public String getScentType() {
        return scentType;
    }
    public void setscentType(String scentType) {
        this.scentType = scentType;
    }
    public String getscentVolume() {
        return scentVolume;
    }
    public void setscentVolume(String scentVolume) {
        this.scentVolume = scentVolume;
    }

    public String toString(){
        return deviceId+" | " + scentType+ " | "+ scentVolume;
    }
}
