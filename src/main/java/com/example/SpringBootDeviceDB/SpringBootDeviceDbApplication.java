package com.example.SpringBootDeviceDB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SpringBootDeviceDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDeviceDbApplication.class, args);
	}
}
