package com.example.SpringBootDeviceDB.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

import java.util.ArrayList;

import org.springframework.jdbc.core.ResultSetExtractor;
import java.sql.ResultSet;
import java.sql.SQLException;

@RestController
public class DeviceController {

    @Autowired
    private DataSource dataSource;


    @GetMapping("/device")
    public List getDevices(Pageable pageable) {
        String sql = "SELECT * FROM devices";
        List deviceList = new ArrayList();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.query(sql, new ResultSetExtractor() {
                    public List extractData(ResultSet rs) throws SQLException {

                        while (rs.next()) {
                            String name = rs.getString("name");
                            String type = rs.getString("scent_type");
                            String volume = rs.getString("scent_volume");
                            deviceList.add(name + " , " + type + " , " + volume);
                        }
                        return deviceList;
                    }
                }
        );
        return deviceList;
    }

    @GetMapping("/device/{id}")
    public List getDeviceByname(@PathVariable String id) {
        String sql = "SELECT * FROM devices where name = '"+id+"'";
        List deviceList = new ArrayList();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.query(sql, new ResultSetExtractor() {
                    public List extractData(ResultSet rs) throws SQLException {

                        while (rs.next()) {
                            String name = rs.getString("name");
                            String type = rs.getString("scent_type");
                            String volume = rs.getString("scent_volume");
                            deviceList.add(name + " , " + type + " , " + volume);
                        }
                        return deviceList;
                    }
                }
        );
        return deviceList;
    }

    @RequestMapping(value = "/device/{id}" , method = RequestMethod.PUT)
    public String UpdateDevice(@PathVariable String id, @RequestParam String type, @RequestParam String volume) {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("update devices set scent_type = '"+type+"', scent_volume = '"+volume+"' where name = '"+id+"'");
        return "Device Updated !!!";
    }

    @RequestMapping(value = "/device/{id}" , method = RequestMethod.DELETE)
    public String DeleteDevice(@PathVariable String id) {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete from devices where name = '"+id+"'");
        return "Device Deleted !!!";
    }

    @RequestMapping(value = "/device" , method = RequestMethod.POST)
    public String UpdateDevice(@RequestParam int id, @RequestParam String name ,@RequestParam String type, @RequestParam String volume) {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("INSERT INTO devices(device_id,name,scent_type,scent_volume)VALUES(?,?,?,?)",id,name,type,volume);
        return "New Device Successfully Added !!!";
    }
}
