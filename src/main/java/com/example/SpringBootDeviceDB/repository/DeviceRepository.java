package com.example.SpringBootDeviceDB.repository;

import com.example.SpringBootDeviceDB.model.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
    List<Device> findByName(String name);
}
