CREATE TABLE scent (
  id              SERIAL PRIMARY KEY,
  scentName           VARCHAR(255) NULL,
  scentType  VARCHAR(255) NULL,
  scentVolume VARCHAR(255) NULL
);