CREATE TABLE devices (
  device_id bigint NOT NULL,
  name           VARCHAR(100) NULL,
  scent_type  VARCHAR(255) NULL,
  scent_volume VARCHAR(255) NULL
);